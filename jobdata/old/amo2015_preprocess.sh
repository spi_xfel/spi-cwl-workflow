#/bin/bash

. /home/thereisalogin/sw/anaconda3/bin/activate

cwl-runner  --cachedir cwl_cache --outdir ../../../amo2015/processed/amo2015_preprocess \
            --timestamps 1>amo2015.out 2>&1 \
            cwl_workflows/amo2015_preprocess.cwl jobdata/amo2015_preprocess_job.yml
            
