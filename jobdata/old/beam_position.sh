#/bin/bash

cwl-runner  --cachedir cwl_cache --outdir beam_position_output cwl_workflows/beam_position.cwl jobdata/beam_position_part1_job.yml
cwl-runner  --cachedir cwl_cache --outdir beam_position_output cwl_workflows/beam_position.cwl jobdata/beam_position_part2_job.yml