#/bin/bash

. /home/thereisalogin/sw/anaconda3/bin/activate

cwl-runner  --outdir ../../../amo2015/processed/amo2015_test_wf \
            --timestamps 1>amo2015_test.out 2>&1 \
            cwl_workflows/amo2015_test_wf.cwl jobdata/amo2015_test_wf_job.yml
            
