You can find documentation for SPI CWL workflows in gitlab Wiki!

Documentation:
* [Installation](https://gitlab.com/spi_xfel/spi-cwl-workflow/-/wikis/Installation)
* [Running analysis](https://gitlab.com/spi_xfel/spi-cwl-workflow/-/wikis/Running%20analysis)
* [Description of CWL documents](https://gitlab.com/spi_xfel/spi-cwl-workflow/-/wikis/CWL%20Description/CWL%20Documents)