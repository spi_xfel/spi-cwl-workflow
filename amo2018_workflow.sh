#/bin/bash

PROC_DATA=/mnt/data/exp/xfel/amo2018/processed/cwl_process

cwl-runner  --cachedir $PROC_DATA/cwl_cache/ \
            --tmpdir-prefix $PROC_DATA/tmp/ \
            --outdir $PROC_DATA/amo2018_out/ \
            --parallel \
            --timestamps 1>$PROC_DATA/amo2018_time.out 2>&1 \
            cwl_workflows/amo2018_workflow.cwl jobdata/amo2018_workflow.yml
