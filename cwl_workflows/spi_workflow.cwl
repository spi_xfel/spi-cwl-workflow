#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}

inputs: 
  input_files: File[]
  estimate_size_r_min: int?
  estimate_size_r_max: int?
  estimate_size_min: int?
  estimate_size_max: int?
  estimate_size_ico: boolean
  refine_center_size_min: float
  refine_center_size_max: float
  select_size_min: float
  select_size_max: float
  select_score_min: float
  wavelength: float
  distance: float
  pixel_size: float
  class_dataset: string
  em_class_num_iter: int
  em_class_qmin: int?
  em_class_qmax: int?
  em_class_num_rot: int?
  em_class_num_class: int?
  em_class_binning: int?
  em_class_symmetry_fringe: int?
  em_class_symmetry_order: int
  dragonfly_num_div: int?
  dragonfly_beta_start: float?
  dragonfly_beta_step: int?
  dragonfly_beta_mul: float?
  dragonfly_min_r: int?
  dragonfly_min_r_soft: int?
  dragonfly_max_r: int?
  dragonfly_binning: int?
  dragonfly_num_iter: int
  emc_correct_mode: string
  emc_correct_limit: int?
  pynx_num_run: int
  pynx_threshold: float?
  pynx_target_size: int
  pynx_algorithm: string?
  pynx_averaging_mode: string?

steps:
  preprocess:
    run: spi_preprocess_workflow.cwl
    in:
      input_files: input_files
      estimate_size_r_min: estimate_size_r_min
      estimate_size_r_max: estimate_size_r_max
      estimate_size_min: estimate_size_min
      estimate_size_max: estimate_size_max
      estimate_size_ico: estimate_size_ico
      refine_center_size_min: refine_center_size_min
      refine_center_size_max: refine_center_size_max
      select_size_min: select_size_min
      select_size_max: select_size_max
      select_score_min: select_score_min
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile, estimate_center_report, refine_center_report, rough_size_hist, size_hist, score_hist]
  em_class:
    run: ../cwl_steps/em_class.cwl
    in:
      input_file: preprocess/outfile
      dataset: class_dataset
      num_iter: em_class_num_iter
      qmin: em_class_qmin
      qmax: em_class_qmax
      num_rot: em_class_num_rot
      num_class: em_class_num_class
      binning: em_class_binning
      symmetry_fringe: em_class_symmetry_fringe
      symmetry_order: em_class_symmetry_order
    out: [outfile, report]
  class_filter:
    run: ../cwl_steps/filter_file.cwl
    in:
      input_file: em_class/outfile
      dataset: class_dataset
      min_value: { default: 1 }
    out: [outfile]
  emc_create:
    run: ../cwl_steps/dragonfly_create.cwl
    in:
      input_file: class_filter/outfile
      num_div: dragonfly_num_div
      beta_start: dragonfly_beta_start
      beta_step: dragonfly_beta_step
      beta_mul: dragonfly_beta_mul
      min_r: dragonfly_min_r
      min_r_soft: dragonfly_min_r_soft
      max_r: dragonfly_max_r
      binning: dragonfly_binning
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outdir]
  emc_run:
    run: ../cwl_steps/dragonfly_run.cwl
    in:
      recon_dir: emc_create/outdir
      num_iter: dragonfly_num_iter
    out: [outdir]
  emc_extract:
    run: ../cwl_steps/dragonfly_extract.cwl
    in:
      recon_dir: emc_run/outdir
    out: [outfile]
  emc_correct:
    run: ../cwl_steps/correct_emc.cwl
    in:
      input_file: emc_extract/outfile
      mode: emc_correct_mode
      limit: emc_correct_limit
    out: [outfile, report]
  pynx_run:
    run: ../cwl_steps/pynx_run.cwl
    in:
      input_file: emc_correct/outfile
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
      input_binning: dragonfly_binning
      num_run: pynx_num_run
      threshold: pynx_threshold
      target_size: pynx_target_size
      algorithm: pynx_algorithm
      averaging_mode: pynx_averaging_mode
    out: [outfile, report]
outputs:
  outfile:
    type: File
    outputSource: pynx_run/outfile
  estimate_center_report:
    type: File
    outputSource: preprocess/estimate_center_report
  refine_center_report:
    type: File
    outputSource: preprocess/refine_center_report
  rough_size_hist:
    type: File
    outputSource: preprocess/rough_size_hist
  size_hist:
    type: File
    outputSource: preprocess/size_hist
  score_hist:
    type: File
    outputSource: preprocess/score_hist
  em_class_report:
    type: File
    outputSource: em_class/report
  emc_correct_report:
    type: File
    outputSource: emc_correct/report
  pynx_report:
    type: File
    outputSource: pynx_run/report
