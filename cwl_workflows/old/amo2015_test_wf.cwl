#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs: 
  rawdir: Directory
  min_psd_range: int?
  max_psd_range: int?
  min_estimate_size: int?
  max_estimate_size: int?
  min_rough_filter_size: float
  max_rough_filter_size: float
  min_fine_filter_size: float
  max_fine_filter_size: float
  min_filter_score: float
  wavelength: float
  distance: float
  pixel_size: float

steps:
  list_input:
    run:
      class: ExpressionTool
      requirements: { InlineJavascriptRequirement: {} }
      inputs:
        dir: Directory
      expression: '${return {"files": inputs.dir.listing};}'
      outputs:
        files: File[]
    in:
      dir: rawdir
    out: [files]
  estimate_center:
    run: ../cwl_steps/estimate_beam_position.cwl
    in:
      input_files: list_input/files
    out: [outfile]
  estimate_rough_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file: estimate_center/outfile
      min_psd_r: min_psd_range
      max_psd_r: max_psd_range
      min_size: min_estimate_size
      max_size: max_estimate_size
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  filter_rough_size:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files:   estimate_rough_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: min_rough_filter_size
      max_value: max_rough_filter_size
    out: [outfile]
  refine_center:
    run: ../cwl_steps/refine_beam_position.cwl
    in:
      input_files: filter_rough_size/outfile
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  select_first_file:
    run:
      class: ExpressionTool
      requirements: { InlineJavascriptRequirement: {} }
      inputs:
        files: File[]
      expression: '${return {"file": inputs.files[0]};}'
      outputs:
        file: File
    in:
      files: refine_center/outfile
    out: [file]
  set_center:
    run: ../cwl_steps/copy_beam_position.cwl
    in:
      input_files: list_input/files
      reference_file:
        select_first_file/file
    out: [outfile]
  estimate_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file: set_center/outfile
      min_psd_r: min_psd_range
      max_psd_r: max_psd_range
      min_size: min_estimate_size
      max_size: max_estimate_size
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  plot_size:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_distribution.pdf
      dataset: 
        valueFrom: psd/size
    out: [outfile]
  plot_score:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_score_distribution.pdf
      dataset: 
        valueFrom: psd/size_score
    out: [outfile]
  filter_size:
    run: ../cwl_steps/filter_file.cwl
    scatter: input_file
    in:
      input_file: estimate_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: min_fine_filter_size
      max_value: max_fine_filter_size
    out: [outfile]
  filter_score:
    run: ../cwl_steps/filter_file.cwl
    scatter: input_file
    in:
      input_file: filter_size/outfile
      dataset:
        valueFrom: psd/size_score
      min_value: min_filter_score
    out: [outfile]
  combine:
    run: ../cwl_steps/combine_data.cwl
    in:
      input_files: filter_score/outfile
      outfile_name: 
        valueFrom: filtered_data.cxi
    out: [outfile]
outputs:
  patterns:
    type: File
    outputSource: combine/outfile
  size_distribution:
    type: File
    outputSource: plot_size/outfile
  score_distribution:
    type: File
    outputSource: plot_score/outfile
