#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}

inputs:
  rawdir:
    type: Directory

steps:
  list_input:
    run:
      class: ExpressionTool
      requirements: { InlineJavascriptRequirement: {} }
      inputs:
        dir: Directory
      expression: '${return {"files": inputs.dir.listing};}'
      outputs:
        files: File[]
    in:
      dir: rawdir
    out: [files]
  corr_bg:
    run: ../cwl_steps/correct_bg.cwl
    scatter: input_file
    in:
      input_file: list_input/files
    out: [outfile]

outputs:
  corr_files:
    type: File[]
    outputSource: corr_bg/outfile