#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}

inputs:
  input_file:
    type: File
  em_data_file:
    type: File
  classes:
    type: int[]
  result_name:
    type: string
  dragonfly_dirname:
    type: string
    default: dragonfly_data
  dragonfly_num_iter:
    type: int
  dragonfly_beta_start:
    type: float
  dragonfly_beta_step:
    type: int
  dragonfly_beta_mul:
    type: float
  dragonfly_min_r:
    type: int
  dragonfly_max_r:
    type: int
  dragonfly_binning:
    type: int
    default: 1
  wavelength:
    type: float
  distance:
    type: float
  pixel_size:
    type: float
steps:
  save_class:
    run: ../cwl_steps/em_save_class.cwl
    in:
      input_file: input_file
      data_file: em_data_file
      outfile_name:
        valueFrom: class_file.cxi
      class_name:
        valueFrom: em_class
      classes: classes
    out: [outfile]
  filter_class:
    run: ../cwl_steps/filter_file.cwl
    in:
      input_file: save_class/outfile
      dataset:
        valueFrom: classification/em_class
      min_value:
        valueFrom: $(1)
    out: [outfile]
  dragonfly_create:
    run: ../cwl_steps/dragonfly_create.cwl
    in:
      input_file: filter_class/outfile
      outdir_name: dragonfly_dirname
      beta_start: dragonfly_beta_start
      beta_step: dragonfly_beta_step
      beta_mul: dragonfly_beta_mul
      min_r: dragonfly_min_r
      max_r:  dragonfly_max_r
      binning: dragonfly_binning
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outdir]
  dragonfly_start:
    run: ../cwl_steps/dragonfly_start.cwl
    in: 
      recon_dir: dragonfly_create/outdir
      outdir_name: dragonfly_dirname
      num_iter: dragonfly_num_iter
    out: [outdir]
  dragonfly_save:
    run: ../cwl_steps/dragonfly_save.cwl
    in:
      recon_dir: dragonfly_start/outdir
      outfile_name: result_name
    out: [outfile]
outputs:
  outfile:
    type: File
    outputSource: dragonfly_save/outfile
  dragonfly_data:
    type: Directory
    outputSource: dragonfly_start/outdir
