#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs: 
  rawdir: Directory
  min_psd_range: int?
  max_psd_range: int?
  min_estimate_size: int?
  max_estimate_size: int?
  min_fine_filter_size: float
  max_fine_filter_size: float
  min_filter_score: float
  wavelength: float
  distance: float
  pixel_size: float
  em_class_data_name: string?
  em_class_num_iter: int
  em_class_qmin: int?
  em_class_qmax: int?
  em_class_num_rot: int?
  em_class_num_class: int?
  em_class_binning: int?

steps:
  list_input:
    run:
      class: ExpressionTool
      requirements: { InlineJavascriptRequirement: {} }
      inputs:
        dir: Directory
      expression: '${return {"files": inputs.dir.listing};}'
      outputs:
        files: File[]
    in:
      dir: rawdir
    out: [files]
  estimate_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file: list_input/files
      min_psd_r: min_psd_range
      max_psd_r: max_psd_range
      min_size: min_estimate_size
      max_size: max_estimate_size
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  plot_size:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_distribution.pdf
      dataset: 
        valueFrom: psd/size
    out: [outfile]
  plot_score:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_score_distribution.pdf
      dataset: 
        valueFrom: psd/size_score
    out: [outfile]
  filter_size:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: estimate_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: min_fine_filter_size
      max_value: max_fine_filter_size
    out: [outfile]
  filter_score:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: filter_size/outfile
      dataset:
        valueFrom: psd/size_score
      min_value: min_filter_score
    out: [outfile]
  combine:
    run: ../cwl_steps/combine_data.cwl
    in:
      input_files: filter_score/outfile
      outfile_name: 
        valueFrom: filtered_data.cxi
    out: [outfile]
  em_run:
    run: ../cwl_steps/em_run.cwl
    in:
      input_file: combine/outfile
      data_name: em_class_data_name
      num_iter: em_class_num_iter
      qmin: em_class_qmin
      qmax: em_class_qmax
      num_rot: em_class_num_rot
      num_class: em_class_num_class
      binning: em_class_binning
    out: [outfile, report]
outputs:
  patterns:
    type: File
    outputSource: combine/outfile
  em_data:
    type: File
    outputSource: em_run/outfile
  em_report:
    type: File
    outputSource: em_run/report
  size_distribution:
    type: File
    outputSource: plot_size/outfile
  score_distribution:
    type: File
    outputSource: plot_score/outfile
