#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs: 
  rawdir: Directory
  shift_run: int
  min_psd_range: int?
  max_psd_range: int?
  min_estimate_size: int?
  max_estimate_size: int?
  min_rough_filter_size: float
  max_rough_filter_size: float
  min_fine_filter_size: float
  max_fine_filter_size: float
  min_filter_score: float
  wavelength: float
  distance: float
  pixel_size: float
  em_class_data_name: string?
  em_class_num_iter: int
  em_class_qmin: int?
  em_class_qmax: int?
  em_class_num_rot: int?
  em_class_num_class: int?
  em_class_binning: int?

steps:
  list_input:
    run:
      class: ExpressionTool
      requirements: { InlineJavascriptRequirement: {} }
      inputs:
        dir: Directory
      expression: '${return {"files": inputs.dir.listing};}'
      outputs:
        files: File[]
    in:
      dir: rawdir
    out: [files]
  split_files_by_run:
    run:
      class: ExpressionTool
      requirements: { InlineJavascriptRequirement: {} }
      inputs:
        files: File[]
        shift_run: int
      expression: |
        ${
        var files_1 = [];
        var files_2 = [];
        inputs.files.forEach(function(item, index, array) {
          var run = parseInt(item.basename.split('.')[0].split('_')[2]);
          if (run < inputs.shift_run){
            files_1.push(item);
          }else{
            files_2.push(item);
          }
        });
        return {"files_1": files_1, "files_2": files_2};
        }
      outputs:
        files_1: File[]
        files_2: File[]
    in:
      files: list_input/files
      shift_run: shift_run
    out: [files_1, files_2]
  correct_bg_1:
    run: ../cwl_steps/correct_bg.cwl
    scatter: input_file
    in:
      input_file: split_files_by_run/files_1
    out: [outfile]
  correct_bg_2:
    run: ../cwl_steps/correct_bg.cwl
    scatter: input_file
    in:
      input_file: split_files_by_run/files_2
    out: [outfile]
  beam_pos_1:
    run: beam_position.cwl
    in:
      input_files: correct_bg_1/outfile
      min_psd_range: min_psd_range
      max_psd_range: max_psd_range
      min_estimate_size: min_estimate_size
      max_estimate_size: max_estimate_size
      min_filter_size: min_rough_filter_size
      max_filter_size: max_rough_filter_size
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  beam_pos_2:
    run: beam_position.cwl
    in:
      input_files: correct_bg_2/outfile
      min_psd_range: min_psd_range
      max_psd_range: max_psd_range
      min_estimate_size: min_estimate_size
      max_estimate_size: max_estimate_size
      min_filter_size: min_rough_filter_size
      max_filter_size: max_rough_filter_size
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  estimate_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file:
        source: [ beam_pos_1/outfile, beam_pos_2/outfile ]
        linkMerge: merge_flattened
      min_psd_r: min_psd_range
      max_psd_r: max_psd_range
      min_size: min_estimate_size
      max_size: max_estimate_size
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  plot_size:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_distribution.pdf
      dataset: 
        valueFrom: psd/size
    out: [outfile]
  plot_score:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_score_distribution.pdf
      dataset: 
        valueFrom: psd/size_score
    out: [outfile]
  filter_size:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: estimate_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: min_fine_filter_size
      max_value: max_fine_filter_size
    out: [outfile]
  filter_score:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: filter_size/outfile
      dataset:
        valueFrom: psd/size_score
      min_value: min_filter_score
    out: [outfile]
  combine:
    run: ../cwl_steps/combine_data.cwl
    in:
      input_files: filter_score/outfile
      outfile_name: 
        valueFrom: filtered_data.cxi
    out: [outfile]
  em_run:
    run: ../cwl_steps/em_run.cwl
    in:
      input_file: combine/outfile
      data_name: em_class_data_name
      num_iter: em_class_num_iter
      qmin: em_class_qmin
      qmax: em_class_qmax
      num_rot: em_class_num_rot
      num_class: em_class_num_class
      binning: em_class_binning
    out: [outfile, report]
outputs:
  patterns:
    type: File
    outputSource: combine/outfile
  em_data:
    type: File
    outputSource: em_run/outfile
  em_report:
    type: File
    outputSource: em_run/report
  size_distribution:
    type: File
    outputSource: plot_size/outfile
  score_distribution:
    type: File
    outputSource: plot_score/outfile
