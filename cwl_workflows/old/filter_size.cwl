#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}

inputs:
  input_dir:
    type: Directory
  output_file: string
  min_psd_range: int?
  max_psd_range: int?
  min_estimate_size: int?
  max_estimate_size: int?
  min_filter_size: float
  max_filter_size: float
  min_filter_score: float
  wavelength: float
  distance: float
  pixel_size: float

steps:
  list_input:
    run:
      class: ExpressionTool
      requirements: { InlineJavascriptRequirement: {} }
      inputs:
        dir: Directory
      expression: '${return {"files": inputs.dir.listing};}'
      outputs:
        files: File[]
    in:
      dir: input_dir
    out: [files]
  estimate_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file: list_input/files
      min_psd_r: min_psd_range
      max_psd_r: max_psd_range
      min_size: min_estimate_size
      max_size: max_estimate_size
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile]
  plot_size:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_distribution.pdf
      dataset: 
        valueFrom: psd/size
    out: [outfile]
  plot_score:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: size_score_distribution.pdf
      dataset: 
        valueFrom: psd/size_score
    out: [outfile]
  filter_size:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: estimate_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: min_filter_size
      max_value: max_filter_size
    out: [outfile]
  filter_score:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: filter_size/outfile
      dataset:
        valueFrom: psd/size_score
      min_value: min_filter_score
    out: [outfile]
  combine:
    run: ../cwl_steps/combine_data.cwl
    in:
      input_files: filter_score/outfile
      outfile_name: output_file
    out: [outfile]

outputs:
  outfile:
    type: File
    outputSource: combine/outfile
  size_distribution:
    type: File
    outputSource: plot_size/outfile
  score_distribution:
    type: File
    outputSource: plot_score/outfile