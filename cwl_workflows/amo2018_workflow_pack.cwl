{
    "$graph": [
        {
            "class": "CommandLineTool",
            "label": "Combine several CXI files into one",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_combine.py",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#combine_data.cwl/input_files"
                },
                {
                    "type": "string",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#combine_data.cwl/outfile_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outfile_name)"
                    },
                    "id": "#combine_data.cwl/outfile"
                }
            ],
            "id": "#combine_data.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Correct background in CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_correct_background.py",
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#correct_bg.cwl/input_file"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "correct_bg_output",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#correct_bg.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "correct_background_report.pdf",
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "id": "#correct_bg.cwl/report_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/$(inputs.input_file.basename)"
                    },
                    "id": "#correct_bg.cwl/outfile"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report_name)"
                    },
                    "id": "#correct_bg.cwl/report"
                }
            ],
            "id": "#correct_bg.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Correct Dragonfly output in CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_correct_amplitude",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_correct.py",
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#correct_emc.cwl/input_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "-l"
                    },
                    "id": "#correct_emc.cwl/limit"
                },
                {
                    "type": "string",
                    "default": "const",
                    "inputBinding": {
                        "prefix": "-m"
                    },
                    "id": "#correct_emc.cwl/mode"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "emc_correct.cxi",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#correct_emc.cwl/outfile_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "correct_emc_report.pdf",
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "id": "#correct_emc.cwl/report_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outfile_name)"
                    },
                    "id": "#correct_emc.cwl/outfile"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report_name)"
                    },
                    "id": "#correct_emc.cwl/report"
                }
            ],
            "id": "#correct_emc.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Create Dragonfly working folder from CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/dragonfly_wrapper",
                    "class": "DockerRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "run.sh",
                            "entry": "dragonfly_create.py $(inputs.outdir_name) --num-div $(inputs.num_div)  --beta $(inputs.beta_start) $(inputs.beta_mul) $(inputs.beta_step) ; dragonfly_add.py $(inputs.input_file.path) $(inputs.outdir_name)  -r $(inputs.min_r) --rsoft $(inputs.min_r_soft) -R $(inputs.max_r)  -b $(inputs.binning) --wavelength $(inputs.wavelength) --distance $(inputs.distance) --pix $(inputs.pixel_size) --hdf ;"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 1.2,
                    "id": "#dragonfly_create.cwl/beta_mul"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0.01,
                    "id": "#dragonfly_create.cwl/beta_start"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 15,
                    "id": "#dragonfly_create.cwl/beta_step"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#dragonfly_create.cwl/binning"
                },
                {
                    "type": "float",
                    "id": "#dragonfly_create.cwl/distance"
                },
                {
                    "type": "File",
                    "id": "#dragonfly_create.cwl/input_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 10000,
                    "id": "#dragonfly_create.cwl/max_r"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#dragonfly_create.cwl/min_r"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#dragonfly_create.cwl/min_r_soft"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 8,
                    "id": "#dragonfly_create.cwl/num_div"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "data_emc",
                    "id": "#dragonfly_create.cwl/outdir_name"
                },
                {
                    "type": "float",
                    "id": "#dragonfly_create.cwl/pixel_size"
                },
                {
                    "type": "float",
                    "id": "#dragonfly_create.cwl/wavelength"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#dragonfly_create.cwl/outdir"
                }
            ],
            "id": "#dragonfly_create.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Save Dragonfly result 3D amplitude into CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/dragonfly_wrapper",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "dragonfly_extract.py",
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "-i"
                    },
                    "id": "#dragonfly_extract.cwl/iter"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "dragonfly_result.cxi",
                    "inputBinding": {
                        "position": 1
                    },
                    "id": "#dragonfly_extract.cwl/outfile_name"
                },
                {
                    "type": "Directory",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#dragonfly_extract.cwl/recon_dir"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outfile_name)"
                    },
                    "id": "#dragonfly_extract.cwl/outfile"
                }
            ],
            "id": "#dragonfly_extract.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Dragonfly computation on input data folder",
            "hints": [
                {
                    "dockerPull": "spixfel/dragonfly",
                    "class": "DockerRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "run.sh",
                            "entry": "cp $(inputs.recon_dir.path) $(inputs.outdir_name) -rf ; cd $(inputs.outdir_name) ; /src/emc -t `nproc` $(inputs.num_iter) ;"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": "int",
                    "id": "#dragonfly_run.cwl/num_iter"
                },
                {
                    "type": "string",
                    "default": "data_emc",
                    "id": "#dragonfly_run.cwl/outdir_name"
                },
                {
                    "type": "Directory",
                    "id": "#dragonfly_run.cwl/recon_dir"
                }
            ],
            "outputs": [
                {
                    "type": "Directory",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)"
                    },
                    "id": "#dragonfly_run.cwl/outdir"
                }
            ],
            "id": "#dragonfly_run.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Perform EM clustering on CXI data",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_em_class",
                    "class": "DockerRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "run.sh",
                            "entry": "spi_em_create.py $(inputs.input_file.path)\nspi_em_config.py --q_min $(inputs.qmin) --q_max $(inputs.qmax) --no-logscale --no-best --friedel --num_rot $(inputs.num_rot) --num_class $(inputs.num_class) --binning $(inputs.binning)\nspi_em_run.py $(inputs.num_iter)\nspi_em_report.py -o $(inputs.report_name)\nspi_em_save_by_symmetry.py $(inputs.symmetry_order) -f $(inputs.symmetry_fringe) -s $(inputs.dataset) -o $(inputs.outfile_name) -t $(inputs.threshold)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#em_class.cwl/binning"
                },
                {
                    "type": "string",
                    "default": "em_class/select",
                    "id": "#em_class.cwl/dataset"
                },
                {
                    "type": "File",
                    "id": "#em_class.cwl/input_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 20,
                    "id": "#em_class.cwl/num_class"
                },
                {
                    "type": "int",
                    "id": "#em_class.cwl/num_iter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 100,
                    "id": "#em_class.cwl/num_rot"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "em_class.cxi",
                    "id": "#em_class.cwl/outfile_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#em_class.cwl/qmax"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#em_class.cwl/qmin"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "em_class_report.pdf",
                    "id": "#em_class.cwl/report_name"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 0,
                    "id": "#em_class.cwl/symmetry_fringe"
                },
                {
                    "type": "int",
                    "id": "#em_class.cwl/symmetry_order"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0.3,
                    "id": "#em_class.cwl/threshold"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outfile_name)"
                    },
                    "id": "#em_class.cwl/outfile"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report_name)"
                    },
                    "id": "#em_class.cwl/report"
                }
            ],
            "id": "#em_class.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Compute beam position in CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_estimate_center.py",
            "inputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#estimate_center.cwl/input_files"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "estimate_center_output",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#estimate_center.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "estimate_center_report.pdf",
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "id": "#estimate_center.cwl/report_name"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "prefix": "-s"
                    },
                    "id": "#estimate_center.cwl/same_position"
                }
            ],
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/*"
                    },
                    "id": "#estimate_center.cwl/outfiles"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report_name)"
                    },
                    "id": "#estimate_center.cwl/report"
                }
            ],
            "id": "#estimate_center.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Estimate particle sizes for images in CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_estimate_size.py",
            "inputs": [
                {
                    "type": "float",
                    "inputBinding": {
                        "prefix": "--distance"
                    },
                    "id": "#estimate_size.cwl/distance"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": false,
                    "inputBinding": {
                        "prefix": "--icosahedron"
                    },
                    "id": "#estimate_size.cwl/icosahedron"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#estimate_size.cwl/input_file"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "-R"
                    },
                    "id": "#estimate_size.cwl/max_psd_r"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "-M"
                    },
                    "id": "#estimate_size.cwl/max_size"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "id": "#estimate_size.cwl/min_psd_r"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "inputBinding": {
                        "prefix": "-m"
                    },
                    "id": "#estimate_size.cwl/min_size"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "estimate_size_output",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#estimate_size.cwl/outdir_name"
                },
                {
                    "type": "float",
                    "inputBinding": {
                        "prefix": "--pix"
                    },
                    "id": "#estimate_size.cwl/pixel_size"
                },
                {
                    "type": "float",
                    "inputBinding": {
                        "prefix": "--wavelength"
                    },
                    "id": "#estimate_size.cwl/wavelength"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/$(inputs.input_file.basename)"
                    },
                    "id": "#estimate_size.cwl/outfile"
                }
            ],
            "id": "#estimate_size.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Filter set of CXI files by min_value <= dataset <= max_value rule.",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_filter.py",
            "inputs": [
                {
                    "type": "string",
                    "inputBinding": {
                        "prefix": "-d"
                    },
                    "id": "#filter_data.cwl/dataset"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#filter_data.cwl/input_files"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "-M"
                    },
                    "id": "#filter_data.cwl/max_value"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "-m"
                    },
                    "id": "#filter_data.cwl/min_value"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "filter_data_output",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#filter_data.cwl/outdir_name"
                }
            ],
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/*"
                    },
                    "id": "#filter_data.cwl/outfiles"
                }
            ],
            "id": "#filter_data.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Filter CXI file by min_value <= dataset <= max_value rule.",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_filter.py",
            "inputs": [
                {
                    "type": "string",
                    "inputBinding": {
                        "prefix": "-d"
                    },
                    "id": "#filter_file.cwl/dataset"
                },
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#filter_file.cwl/input_file"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "-M"
                    },
                    "id": "#filter_file.cwl/max_value"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "-m"
                    },
                    "id": "#filter_file.cwl/min_value"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "filter_data_output",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#filter_file.cwl/outdir_name"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/$(inputs.input_file.basename)"
                    },
                    "id": "#filter_file.cwl/outfile"
                }
            ],
            "id": "#filter_file.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Plot histogram of values in dataset in CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_plot_histogram.py",
            "inputs": [
                {
                    "type": "string",
                    "inputBinding": {
                        "prefix": "-d"
                    },
                    "id": "#plot_histogram.cwl/dataset"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#plot_histogram.cwl/input_files"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "histogram.pdf",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#plot_histogram.cwl/outfile_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "inputBinding": {
                        "prefix": "-s"
                    },
                    "id": "#plot_histogram.cwl/selection"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outfile_name)"
                    },
                    "id": "#plot_histogram.cwl/outfile"
                }
            ],
            "id": "#plot_histogram.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Phase retrieval for CXI data with PYNX lib",
            "hints": [
                {
                    "dockerPull": "spixfel/pynx-wrapper",
                    "class": "DockerRequirement"
                }
            ],
            "requirements": [
                {
                    "listing": [
                        {
                            "entryname": "run.sh",
                            "entry": "pynx_create.py --data $(inputs.input_file.path) --nb_run $(inputs.num_run) \\ --detector_distance $(inputs.distance) --wavelength $(inputs.wavelength) \\ --pixel_size_detector $(inputs.pixel_size) --input_binning $(inputs.input_binning) \\ -o $(inputs.params_name) --support_threshold $(inputs.threshold) \\ --target_size $(inputs.target_size) --algorithm \"$(inputs.algorithm)\" && \\ pynx-id01cdi.py $(inputs.params_name) && \\ rm latest.cxi && \\ pynx_mean_result.py *.cxi -o $(inputs.outfile_name) --mode $(inputs.averaging_mode) -r $(inputs.report_name)"
                        }
                    ],
                    "class": "InitialWorkDirRequirement"
                }
            ],
            "baseCommand": [
                "/bin/sh",
                "run.sh"
            ],
            "inputs": [
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "(Sup*ER**10) ** 20 * (Sup*HIO**90) * (Sup*ER**10) ** 20 * (Sup*HIO**90) * (Sup*ER**10) ** 20 * (Sup*HIO**90)",
                    "id": "#pynx_run.cwl/algorithm"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "mean_real",
                    "id": "#pynx_run.cwl/averaging_mode"
                },
                {
                    "type": "float",
                    "id": "#pynx_run.cwl/distance"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "default": 1,
                    "id": "#pynx_run.cwl/input_binning"
                },
                {
                    "type": "File",
                    "id": "#pynx_run.cwl/input_file"
                },
                {
                    "type": "int",
                    "id": "#pynx_run.cwl/num_run"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "result.cxi",
                    "id": "#pynx_run.cwl/outfile_name"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "pynx_params.txt",
                    "id": "#pynx_run.cwl/params_name"
                },
                {
                    "type": "float",
                    "id": "#pynx_run.cwl/pixel_size"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "phase_retrieval.pdf",
                    "id": "#pynx_run.cwl/report_name"
                },
                {
                    "type": "int",
                    "id": "#pynx_run.cwl/target_size"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0.25,
                    "id": "#pynx_run.cwl/threshold"
                },
                {
                    "type": "float",
                    "id": "#pynx_run.cwl/wavelength"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outfile_name)"
                    },
                    "id": "#pynx_run.cwl/outfile"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report_name)"
                    },
                    "id": "#pynx_run.cwl/report"
                }
            ],
            "id": "#pynx_run.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Refine beam position in CXI file with sphere scattering pattern",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_refine_center.py",
            "inputs": [
                {
                    "type": "float",
                    "inputBinding": {
                        "prefix": "--distance"
                    },
                    "id": "#refine_center.cwl/distance"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#refine_center.cwl/input_files"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "refine_center_output",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#refine_center.cwl/outdir_name"
                },
                {
                    "type": "float",
                    "inputBinding": {
                        "prefix": "--pix"
                    },
                    "id": "#refine_center.cwl/pixel_size"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "refine_center_report.pdf",
                    "inputBinding": {
                        "prefix": "-r"
                    },
                    "id": "#refine_center.cwl/report_name"
                },
                {
                    "type": [
                        "null",
                        "boolean"
                    ],
                    "default": true,
                    "inputBinding": {
                        "prefix": "-s"
                    },
                    "id": "#refine_center.cwl/same_position"
                },
                {
                    "type": "float",
                    "inputBinding": {
                        "prefix": "--wavelength"
                    },
                    "id": "#refine_center.cwl/wavelength"
                }
            ],
            "outputs": [
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/*"
                    },
                    "id": "#refine_center.cwl/outfiles"
                },
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.report_name)"
                    },
                    "id": "#refine_center.cwl/report"
                }
            ],
            "id": "#refine_center.cwl"
        },
        {
            "class": "CommandLineTool",
            "label": "Set beam position in CXI file",
            "hints": [
                {
                    "dockerPull": "spixfel/spi_processing",
                    "class": "DockerRequirement"
                }
            ],
            "baseCommand": "spi_set_center.py",
            "inputs": [
                {
                    "type": "File",
                    "inputBinding": {
                        "position": 0
                    },
                    "id": "#set_center.cwl/input_file"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "default": "set_center_output",
                    "inputBinding": {
                        "prefix": "-o"
                    },
                    "id": "#set_center.cwl/outdir_name"
                },
                {
                    "type": [
                        "null",
                        "File"
                    ],
                    "inputBinding": {
                        "prefix": "-i"
                    },
                    "id": "#set_center.cwl/reference_file"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "-x"
                    },
                    "id": "#set_center.cwl/x"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "-y"
                    },
                    "id": "#set_center.cwl/y"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "inputBinding": {
                        "prefix": "-z"
                    },
                    "id": "#set_center.cwl/z"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputBinding": {
                        "glob": "$(inputs.outdir_name)/$(inputs.input_file.basename)"
                    },
                    "id": "#set_center.cwl/outfile"
                }
            ],
            "id": "#set_center.cwl"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "MultipleInputFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "string",
                    "id": "#main/class_dataset"
                },
                {
                    "type": "float",
                    "id": "#main/distance"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "id": "#main/dragonfly_beta_mul"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "id": "#main/dragonfly_beta_start"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/dragonfly_beta_step"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/dragonfly_binning"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/dragonfly_max_r"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/dragonfly_min_r"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/dragonfly_min_r_soft"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/dragonfly_num_div"
                },
                {
                    "type": "int",
                    "id": "#main/dragonfly_num_iter"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "default": 0.3,
                    "id": "#main/em_clash_thresh"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/em_class_binning"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/em_class_num_class"
                },
                {
                    "type": "int",
                    "id": "#main/em_class_num_iter"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/em_class_num_rot"
                },
                {
                    "type": "int",
                    "id": "#main/em_class_qmax"
                },
                {
                    "type": "int",
                    "id": "#main/em_class_qmin"
                },
                {
                    "type": "int",
                    "id": "#main/em_class_symmetry_order"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/emc_correct_limit"
                },
                {
                    "type": "string",
                    "id": "#main/emc_correct_mode"
                },
                {
                    "type": "boolean",
                    "id": "#main/estimate_size_ico"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/estimate_size_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/estimate_size_min"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/estimate_size_r_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#main/estimate_size_r_min"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#main/input_files_1"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#main/input_files_2"
                },
                {
                    "type": "float",
                    "id": "#main/pixel_size"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "id": "#main/pynx_algorithm"
                },
                {
                    "type": [
                        "null",
                        "string"
                    ],
                    "id": "#main/pynx_averaging_mode"
                },
                {
                    "type": "int",
                    "id": "#main/pynx_num_run"
                },
                {
                    "type": "int",
                    "id": "#main/pynx_target_size"
                },
                {
                    "type": [
                        "null",
                        "float"
                    ],
                    "id": "#main/pynx_threshold"
                },
                {
                    "type": "float",
                    "id": "#main/refine_center_size_max"
                },
                {
                    "type": "float",
                    "id": "#main/refine_center_size_min"
                },
                {
                    "type": "float",
                    "id": "#main/select_score_min"
                },
                {
                    "type": "float",
                    "id": "#main/select_size_max"
                },
                {
                    "type": "float",
                    "id": "#main/select_size_min"
                },
                {
                    "type": "float",
                    "id": "#main/wavelength"
                }
            ],
            "steps": [
                {
                    "run": "#filter_file.cwl",
                    "in": [
                        {
                            "source": "#main/class_dataset",
                            "id": "#main/class_filter/dataset"
                        },
                        {
                            "source": "#main/em_class/outfile",
                            "id": "#main/class_filter/input_file"
                        },
                        {
                            "default": 1,
                            "id": "#main/class_filter/min_value"
                        }
                    ],
                    "out": [
                        "#main/class_filter/outfile"
                    ],
                    "id": "#main/class_filter"
                },
                {
                    "run": "#combine_data.cwl",
                    "in": [
                        {
                            "source": [
                                "#main/preprocess_1/outfile",
                                "#main/preprocess_2/outfile"
                            ],
                            "linkMerge": "merge_flattened",
                            "id": "#main/combine/input_files"
                        },
                        {
                            "valueFrom": "particle_size.cxi",
                            "id": "#main/combine/outfile_name"
                        }
                    ],
                    "out": [
                        "#main/combine/outfile"
                    ],
                    "id": "#main/combine"
                },
                {
                    "run": "#em_class.cwl",
                    "in": [
                        {
                            "source": "#main/em_class_binning",
                            "id": "#main/em_class/binning"
                        },
                        {
                            "source": "#main/class_dataset",
                            "id": "#main/em_class/dataset"
                        },
                        {
                            "source": "#main/combine/outfile",
                            "id": "#main/em_class/input_file"
                        },
                        {
                            "source": "#main/em_class_num_class",
                            "id": "#main/em_class/num_class"
                        },
                        {
                            "source": "#main/em_class_num_iter",
                            "id": "#main/em_class/num_iter"
                        },
                        {
                            "source": "#main/em_class_num_rot",
                            "id": "#main/em_class/num_rot"
                        },
                        {
                            "source": "#main/em_class_qmax",
                            "id": "#main/em_class/qmax"
                        },
                        {
                            "source": "#main/em_class_qmin",
                            "id": "#main/em_class/qmin"
                        },
                        {
                            "source": "#main/em_class_symmetry_order",
                            "id": "#main/em_class/symmetry_order"
                        },
                        {
                            "source": "#main/em_clash_thresh",
                            "id": "#main/em_class/threshold"
                        }
                    ],
                    "out": [
                        "#main/em_class/outfile",
                        "#main/em_class/report"
                    ],
                    "id": "#main/em_class"
                },
                {
                    "run": "#correct_emc.cwl",
                    "in": [
                        {
                            "source": "#main/emc_extract/outfile",
                            "id": "#main/emc_correct/input_file"
                        },
                        {
                            "source": "#main/emc_correct_limit",
                            "id": "#main/emc_correct/limit"
                        },
                        {
                            "source": "#main/emc_correct_mode",
                            "id": "#main/emc_correct/mode"
                        }
                    ],
                    "out": [
                        "#main/emc_correct/outfile",
                        "#main/emc_correct/report"
                    ],
                    "id": "#main/emc_correct"
                },
                {
                    "run": "#dragonfly_create.cwl",
                    "in": [
                        {
                            "source": "#main/dragonfly_beta_mul",
                            "id": "#main/emc_create/beta_mul"
                        },
                        {
                            "source": "#main/dragonfly_beta_start",
                            "id": "#main/emc_create/beta_start"
                        },
                        {
                            "source": "#main/dragonfly_beta_step",
                            "id": "#main/emc_create/beta_step"
                        },
                        {
                            "source": "#main/dragonfly_binning",
                            "id": "#main/emc_create/binning"
                        },
                        {
                            "source": "#main/distance",
                            "id": "#main/emc_create/distance"
                        },
                        {
                            "source": "#main/class_filter/outfile",
                            "id": "#main/emc_create/input_file"
                        },
                        {
                            "source": "#main/dragonfly_max_r",
                            "id": "#main/emc_create/max_r"
                        },
                        {
                            "source": "#main/dragonfly_min_r",
                            "id": "#main/emc_create/min_r"
                        },
                        {
                            "source": "#main/dragonfly_min_r_soft",
                            "id": "#main/emc_create/min_r_soft"
                        },
                        {
                            "source": "#main/dragonfly_num_div",
                            "id": "#main/emc_create/num_div"
                        },
                        {
                            "source": "#main/pixel_size",
                            "id": "#main/emc_create/pixel_size"
                        },
                        {
                            "source": "#main/wavelength",
                            "id": "#main/emc_create/wavelength"
                        }
                    ],
                    "out": [
                        "#main/emc_create/outdir"
                    ],
                    "id": "#main/emc_create"
                },
                {
                    "run": "#dragonfly_extract.cwl",
                    "in": [
                        {
                            "source": "#main/emc_run/outdir",
                            "id": "#main/emc_extract/recon_dir"
                        }
                    ],
                    "out": [
                        "#main/emc_extract/outfile"
                    ],
                    "id": "#main/emc_extract"
                },
                {
                    "run": "#dragonfly_run.cwl",
                    "in": [
                        {
                            "source": "#main/dragonfly_num_iter",
                            "id": "#main/emc_run/num_iter"
                        },
                        {
                            "source": "#main/emc_create/outdir",
                            "id": "#main/emc_run/recon_dir"
                        }
                    ],
                    "out": [
                        "#main/emc_run/outdir"
                    ],
                    "id": "#main/emc_run"
                },
                {
                    "run": "#spi_preprocess_workflow.cwl",
                    "in": [
                        {
                            "source": "#main/distance",
                            "id": "#main/preprocess_1/distance"
                        },
                        {
                            "source": "#main/estimate_size_ico",
                            "id": "#main/preprocess_1/estimate_size_ico"
                        },
                        {
                            "source": "#main/estimate_size_max",
                            "id": "#main/preprocess_1/estimate_size_max"
                        },
                        {
                            "source": "#main/estimate_size_min",
                            "id": "#main/preprocess_1/estimate_size_min"
                        },
                        {
                            "source": "#main/estimate_size_r_max",
                            "id": "#main/preprocess_1/estimate_size_r_max"
                        },
                        {
                            "source": "#main/estimate_size_r_min",
                            "id": "#main/preprocess_1/estimate_size_r_min"
                        },
                        {
                            "source": "#main/input_files_1",
                            "id": "#main/preprocess_1/input_files"
                        },
                        {
                            "source": "#main/pixel_size",
                            "id": "#main/preprocess_1/pixel_size"
                        },
                        {
                            "source": "#main/refine_center_size_max",
                            "id": "#main/preprocess_1/refine_center_size_max"
                        },
                        {
                            "source": "#main/refine_center_size_min",
                            "id": "#main/preprocess_1/refine_center_size_min"
                        },
                        {
                            "source": "#main/select_score_min",
                            "id": "#main/preprocess_1/select_score_min"
                        },
                        {
                            "source": "#main/select_size_max",
                            "id": "#main/preprocess_1/select_size_max"
                        },
                        {
                            "source": "#main/select_size_min",
                            "id": "#main/preprocess_1/select_size_min"
                        },
                        {
                            "source": "#main/wavelength",
                            "id": "#main/preprocess_1/wavelength"
                        }
                    ],
                    "out": [
                        "#main/preprocess_1/outfile",
                        "#main/preprocess_1/estimate_center_report",
                        "#main/preprocess_1/refine_center_report",
                        "#main/preprocess_1/rough_size_hist",
                        "#main/preprocess_1/size_hist",
                        "#main/preprocess_1/score_hist"
                    ],
                    "id": "#main/preprocess_1"
                },
                {
                    "run": "#spi_preprocess_workflow.cwl",
                    "in": [
                        {
                            "source": "#main/distance",
                            "id": "#main/preprocess_2/distance"
                        },
                        {
                            "source": "#main/estimate_size_ico",
                            "id": "#main/preprocess_2/estimate_size_ico"
                        },
                        {
                            "source": "#main/estimate_size_max",
                            "id": "#main/preprocess_2/estimate_size_max"
                        },
                        {
                            "source": "#main/estimate_size_min",
                            "id": "#main/preprocess_2/estimate_size_min"
                        },
                        {
                            "source": "#main/estimate_size_r_max",
                            "id": "#main/preprocess_2/estimate_size_r_max"
                        },
                        {
                            "source": "#main/estimate_size_r_min",
                            "id": "#main/preprocess_2/estimate_size_r_min"
                        },
                        {
                            "source": "#main/input_files_2",
                            "id": "#main/preprocess_2/input_files"
                        },
                        {
                            "source": "#main/pixel_size",
                            "id": "#main/preprocess_2/pixel_size"
                        },
                        {
                            "source": "#main/refine_center_size_max",
                            "id": "#main/preprocess_2/refine_center_size_max"
                        },
                        {
                            "source": "#main/refine_center_size_min",
                            "id": "#main/preprocess_2/refine_center_size_min"
                        },
                        {
                            "source": "#main/select_score_min",
                            "id": "#main/preprocess_2/select_score_min"
                        },
                        {
                            "source": "#main/select_size_max",
                            "id": "#main/preprocess_2/select_size_max"
                        },
                        {
                            "source": "#main/select_size_min",
                            "id": "#main/preprocess_2/select_size_min"
                        },
                        {
                            "source": "#main/wavelength",
                            "id": "#main/preprocess_2/wavelength"
                        }
                    ],
                    "out": [
                        "#main/preprocess_2/outfile",
                        "#main/preprocess_2/estimate_center_report",
                        "#main/preprocess_2/refine_center_report",
                        "#main/preprocess_2/rough_size_hist",
                        "#main/preprocess_2/size_hist",
                        "#main/preprocess_2/score_hist"
                    ],
                    "id": "#main/preprocess_2"
                },
                {
                    "run": "#pynx_run.cwl",
                    "in": [
                        {
                            "source": "#main/pynx_algorithm",
                            "id": "#main/pynx_run/algorithm"
                        },
                        {
                            "source": "#main/pynx_averaging_mode",
                            "id": "#main/pynx_run/averaging_mode"
                        },
                        {
                            "source": "#main/distance",
                            "id": "#main/pynx_run/distance"
                        },
                        {
                            "source": "#main/dragonfly_binning",
                            "id": "#main/pynx_run/input_binning"
                        },
                        {
                            "source": "#main/emc_correct/outfile",
                            "id": "#main/pynx_run/input_file"
                        },
                        {
                            "source": "#main/pynx_num_run",
                            "id": "#main/pynx_run/num_run"
                        },
                        {
                            "source": "#main/pixel_size",
                            "id": "#main/pynx_run/pixel_size"
                        },
                        {
                            "source": "#main/pynx_target_size",
                            "id": "#main/pynx_run/target_size"
                        },
                        {
                            "source": "#main/pynx_threshold",
                            "id": "#main/pynx_run/threshold"
                        },
                        {
                            "source": "#main/wavelength",
                            "id": "#main/pynx_run/wavelength"
                        }
                    ],
                    "out": [
                        "#main/pynx_run/outfile",
                        "#main/pynx_run/report"
                    ],
                    "id": "#main/pynx_run"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputSource": "#main/em_class/report",
                    "id": "#main/em_class_report"
                },
                {
                    "type": "File",
                    "outputSource": "#main/emc_correct/report",
                    "id": "#main/emc_correct_report"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_1/estimate_center_report",
                    "id": "#main/estimate_center_report_1"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_2/estimate_center_report",
                    "id": "#main/estimate_center_report_2"
                },
                {
                    "type": "File",
                    "outputSource": "#main/pynx_run/outfile",
                    "id": "#main/outfile"
                },
                {
                    "type": "File",
                    "outputSource": "#main/pynx_run/report",
                    "id": "#main/pynx_report"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_1/refine_center_report",
                    "id": "#main/refine_center_report_1"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_2/refine_center_report",
                    "id": "#main/refine_center_report_2"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_1/rough_size_hist",
                    "id": "#main/rough_size_hist_1"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_2/rough_size_hist",
                    "id": "#main/rough_size_hist_2"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_1/score_hist",
                    "id": "#main/score_hist_1"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_2/score_hist",
                    "id": "#main/score_hist_2"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_1/size_hist",
                    "id": "#main/size_hist_1"
                },
                {
                    "type": "File",
                    "outputSource": "#main/preprocess_2/size_hist",
                    "id": "#main/size_hist_2"
                }
            ],
            "id": "#main"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "float",
                    "id": "#image_center.cwl/distance"
                },
                {
                    "type": "boolean",
                    "id": "#image_center.cwl/estimate_size_ico"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#image_center.cwl/estimate_size_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#image_center.cwl/estimate_size_min"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#image_center.cwl/estimate_size_r_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#image_center.cwl/estimate_size_r_min"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#image_center.cwl/input_files"
                },
                {
                    "type": "float",
                    "id": "#image_center.cwl/pixel_size"
                },
                {
                    "type": "float",
                    "id": "#image_center.cwl/refine_center_size_max"
                },
                {
                    "type": "float",
                    "id": "#image_center.cwl/refine_center_size_min"
                },
                {
                    "type": "float",
                    "id": "#image_center.cwl/wavelength"
                }
            ],
            "steps": [
                {
                    "run": "#estimate_center.cwl",
                    "in": [
                        {
                            "source": "#image_center.cwl/input_files",
                            "id": "#image_center.cwl/estimate_center/input_files"
                        }
                    ],
                    "out": [
                        "#image_center.cwl/estimate_center/outfiles",
                        "#image_center.cwl/estimate_center/report"
                    ],
                    "id": "#image_center.cwl/estimate_center"
                },
                {
                    "run": "#estimate_size.cwl",
                    "scatter": "#image_center.cwl/estimate_size/input_file",
                    "in": [
                        {
                            "source": "#image_center.cwl/distance",
                            "id": "#image_center.cwl/estimate_size/distance"
                        },
                        {
                            "source": "#image_center.cwl/estimate_size_ico",
                            "id": "#image_center.cwl/estimate_size/icosahedron"
                        },
                        {
                            "source": "#image_center.cwl/estimate_center/outfiles",
                            "id": "#image_center.cwl/estimate_size/input_file"
                        },
                        {
                            "source": "#image_center.cwl/estimate_size_r_max",
                            "id": "#image_center.cwl/estimate_size/max_psd_r"
                        },
                        {
                            "source": "#image_center.cwl/estimate_size_max",
                            "id": "#image_center.cwl/estimate_size/max_size"
                        },
                        {
                            "source": "#image_center.cwl/estimate_size_r_min",
                            "id": "#image_center.cwl/estimate_size/min_psd_r"
                        },
                        {
                            "source": "#image_center.cwl/estimate_size_min",
                            "id": "#image_center.cwl/estimate_size/min_size"
                        },
                        {
                            "source": "#image_center.cwl/pixel_size",
                            "id": "#image_center.cwl/estimate_size/pixel_size"
                        },
                        {
                            "source": "#image_center.cwl/wavelength",
                            "id": "#image_center.cwl/estimate_size/wavelength"
                        }
                    ],
                    "out": [
                        "#image_center.cwl/estimate_size/outfile"
                    ],
                    "id": "#image_center.cwl/estimate_size"
                },
                {
                    "run": "#filter_data.cwl",
                    "in": [
                        {
                            "valueFrom": "psd/size",
                            "id": "#image_center.cwl/filter_size/dataset"
                        },
                        {
                            "source": "#image_center.cwl/estimate_size/outfile",
                            "id": "#image_center.cwl/filter_size/input_files"
                        },
                        {
                            "source": "#image_center.cwl/refine_center_size_max",
                            "id": "#image_center.cwl/filter_size/max_value"
                        },
                        {
                            "source": "#image_center.cwl/refine_center_size_min",
                            "id": "#image_center.cwl/filter_size/min_value"
                        }
                    ],
                    "out": [
                        "#image_center.cwl/filter_size/outfiles"
                    ],
                    "id": "#image_center.cwl/filter_size"
                },
                {
                    "run": "#plot_histogram.cwl",
                    "in": [
                        {
                            "valueFrom": "psd/size",
                            "id": "#image_center.cwl/plot_histogram/dataset"
                        },
                        {
                            "source": "#image_center.cwl/estimate_size/outfile",
                            "id": "#image_center.cwl/plot_histogram/input_files"
                        },
                        {
                            "valueFrom": "particle_size_rough.pdf",
                            "id": "#image_center.cwl/plot_histogram/outfile_name"
                        },
                        {
                            "source": "#image_center.cwl/refine_center_size_max",
                            "id": "#image_center.cwl/plot_histogram/select_max"
                        },
                        {
                            "source": "#image_center.cwl/refine_center_size_min",
                            "id": "#image_center.cwl/plot_histogram/select_min"
                        },
                        {
                            "valueFrom": "$(inputs.select_min + \":\" + inputs.select_max)",
                            "id": "#image_center.cwl/plot_histogram/selection"
                        }
                    ],
                    "out": [
                        "#image_center.cwl/plot_histogram/outfile"
                    ],
                    "id": "#image_center.cwl/plot_histogram"
                },
                {
                    "run": "#refine_center.cwl",
                    "in": [
                        {
                            "source": "#image_center.cwl/distance",
                            "id": "#image_center.cwl/refine_center/distance"
                        },
                        {
                            "source": "#image_center.cwl/filter_size/outfiles",
                            "id": "#image_center.cwl/refine_center/input_files"
                        },
                        {
                            "source": "#image_center.cwl/pixel_size",
                            "id": "#image_center.cwl/refine_center/pixel_size"
                        },
                        {
                            "source": "#image_center.cwl/wavelength",
                            "id": "#image_center.cwl/refine_center/wavelength"
                        }
                    ],
                    "out": [
                        "#image_center.cwl/refine_center/outfiles",
                        "#image_center.cwl/refine_center/report"
                    ],
                    "id": "#image_center.cwl/refine_center"
                },
                {
                    "run": {
                        "class": "ExpressionTool",
                        "inputs": [
                            {
                                "type": {
                                    "type": "array",
                                    "items": "File"
                                },
                                "id": "#image_center.cwl/select_first_file/97e9e12c-5aa2-4aaa-af02-123b37ddb66e/files"
                            }
                        ],
                        "expression": "${return {\"file\": inputs.files[0]};}",
                        "outputs": [
                            {
                                "type": "File",
                                "id": "#image_center.cwl/select_first_file/97e9e12c-5aa2-4aaa-af02-123b37ddb66e/file"
                            }
                        ],
                        "id": "#image_center.cwl/select_first_file/97e9e12c-5aa2-4aaa-af02-123b37ddb66e"
                    },
                    "in": [
                        {
                            "source": "#image_center.cwl/refine_center/outfiles",
                            "id": "#image_center.cwl/select_first_file/files"
                        }
                    ],
                    "out": [
                        "#image_center.cwl/select_first_file/file"
                    ],
                    "id": "#image_center.cwl/select_first_file"
                },
                {
                    "run": "#set_center.cwl",
                    "scatter": "#image_center.cwl/set_center/input_file",
                    "in": [
                        {
                            "source": "#image_center.cwl/input_files",
                            "id": "#image_center.cwl/set_center/input_file"
                        },
                        {
                            "source": "#image_center.cwl/select_first_file/file",
                            "id": "#image_center.cwl/set_center/reference_file"
                        }
                    ],
                    "out": [
                        "#image_center.cwl/set_center/outfile"
                    ],
                    "id": "#image_center.cwl/set_center"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputSource": "#image_center.cwl/estimate_center/report",
                    "id": "#image_center.cwl/estimate_center_report"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "outputSource": "#image_center.cwl/set_center/outfile",
                    "id": "#image_center.cwl/outfiles"
                },
                {
                    "type": "File",
                    "outputSource": "#image_center.cwl/refine_center/report",
                    "id": "#image_center.cwl/refine_center_report"
                },
                {
                    "type": "File",
                    "outputSource": "#image_center.cwl/plot_histogram/outfile",
                    "id": "#image_center.cwl/rough_size_hist"
                }
            ],
            "id": "#image_center.cwl"
        },
        {
            "class": "Workflow",
            "requirements": [
                {
                    "class": "InlineJavascriptRequirement"
                },
                {
                    "class": "ScatterFeatureRequirement"
                },
                {
                    "class": "StepInputExpressionRequirement"
                },
                {
                    "class": "SubworkflowFeatureRequirement"
                }
            ],
            "inputs": [
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/distance"
                },
                {
                    "type": "boolean",
                    "id": "#spi_preprocess_workflow.cwl/estimate_size_ico"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/estimate_size_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/estimate_size_min"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/estimate_size_r_max"
                },
                {
                    "type": [
                        "null",
                        "int"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/estimate_size_r_min"
                },
                {
                    "type": {
                        "type": "array",
                        "items": "File"
                    },
                    "id": "#spi_preprocess_workflow.cwl/input_files"
                },
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/pixel_size"
                },
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/refine_center_size_max"
                },
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/refine_center_size_min"
                },
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/select_score_min"
                },
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/select_size_max"
                },
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/select_size_min"
                },
                {
                    "type": "float",
                    "id": "#spi_preprocess_workflow.cwl/wavelength"
                }
            ],
            "steps": [
                {
                    "run": "#combine_data.cwl",
                    "in": [
                        {
                            "source": "#spi_preprocess_workflow.cwl/filter_score/outfiles",
                            "id": "#spi_preprocess_workflow.cwl/combine/input_files"
                        },
                        {
                            "valueFrom": "particle_size.cxi",
                            "id": "#spi_preprocess_workflow.cwl/combine/outfile_name"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/combine/outfile"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/combine"
                },
                {
                    "run": "#correct_bg.cwl",
                    "scatter": "#spi_preprocess_workflow.cwl/correct_bg/input_file",
                    "in": [
                        {
                            "source": "#spi_preprocess_workflow.cwl/input_files",
                            "id": "#spi_preprocess_workflow.cwl/correct_bg/input_file"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/correct_bg/outfile",
                        "#spi_preprocess_workflow.cwl/correct_bg/report"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/correct_bg"
                },
                {
                    "run": "#estimate_size.cwl",
                    "scatter": "#spi_preprocess_workflow.cwl/estimate_size/input_file",
                    "in": [
                        {
                            "source": "#spi_preprocess_workflow.cwl/distance",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/distance"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_ico",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/icosahedron"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/image_center/outfiles",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/input_file"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_r_max",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/max_psd_r"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_max",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/max_size"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_r_min",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/min_psd_r"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_min",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/min_size"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/pixel_size",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/pixel_size"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/wavelength",
                            "id": "#spi_preprocess_workflow.cwl/estimate_size/wavelength"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/estimate_size/outfile"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/estimate_size"
                },
                {
                    "run": "#filter_data.cwl",
                    "in": [
                        {
                            "valueFrom": "psd/size_score",
                            "id": "#spi_preprocess_workflow.cwl/filter_score/dataset"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/filter_size/outfiles",
                            "id": "#spi_preprocess_workflow.cwl/filter_score/input_files"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/select_score_min",
                            "id": "#spi_preprocess_workflow.cwl/filter_score/min_value"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/filter_score/outfiles"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/filter_score"
                },
                {
                    "run": "#filter_data.cwl",
                    "in": [
                        {
                            "valueFrom": "psd/size",
                            "id": "#spi_preprocess_workflow.cwl/filter_size/dataset"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size/outfile",
                            "id": "#spi_preprocess_workflow.cwl/filter_size/input_files"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/select_size_max",
                            "id": "#spi_preprocess_workflow.cwl/filter_size/max_value"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/select_size_min",
                            "id": "#spi_preprocess_workflow.cwl/filter_size/min_value"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/filter_size/outfiles"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/filter_size"
                },
                {
                    "run": "#image_center.cwl",
                    "in": [
                        {
                            "source": "#spi_preprocess_workflow.cwl/distance",
                            "id": "#spi_preprocess_workflow.cwl/image_center/distance"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_ico",
                            "id": "#spi_preprocess_workflow.cwl/image_center/estimate_size_ico"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_max",
                            "id": "#spi_preprocess_workflow.cwl/image_center/estimate_size_max"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_min",
                            "id": "#spi_preprocess_workflow.cwl/image_center/estimate_size_min"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_r_max",
                            "id": "#spi_preprocess_workflow.cwl/image_center/estimate_size_r_max"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size_r_min",
                            "id": "#spi_preprocess_workflow.cwl/image_center/estimate_size_r_min"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/correct_bg/outfile",
                            "id": "#spi_preprocess_workflow.cwl/image_center/input_files"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/pixel_size",
                            "id": "#spi_preprocess_workflow.cwl/image_center/pixel_size"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/refine_center_size_max",
                            "id": "#spi_preprocess_workflow.cwl/image_center/refine_center_size_max"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/refine_center_size_min",
                            "id": "#spi_preprocess_workflow.cwl/image_center/refine_center_size_min"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/wavelength",
                            "id": "#spi_preprocess_workflow.cwl/image_center/wavelength"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/image_center/outfiles",
                        "#spi_preprocess_workflow.cwl/image_center/estimate_center_report",
                        "#spi_preprocess_workflow.cwl/image_center/refine_center_report",
                        "#spi_preprocess_workflow.cwl/image_center/rough_size_hist"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/image_center"
                },
                {
                    "run": "#plot_histogram.cwl",
                    "in": [
                        {
                            "valueFrom": "psd/size_score",
                            "id": "#spi_preprocess_workflow.cwl/plot_score/dataset"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size/outfile",
                            "id": "#spi_preprocess_workflow.cwl/plot_score/input_files"
                        },
                        {
                            "valueFrom": "particle_size_score.pdf",
                            "id": "#spi_preprocess_workflow.cwl/plot_score/outfile_name"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/plot_score/outfile"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/plot_score"
                },
                {
                    "run": "#plot_histogram.cwl",
                    "in": [
                        {
                            "valueFrom": "psd/size",
                            "id": "#spi_preprocess_workflow.cwl/plot_size/dataset"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/estimate_size/outfile",
                            "id": "#spi_preprocess_workflow.cwl/plot_size/input_files"
                        },
                        {
                            "valueFrom": "particle_size.pdf",
                            "id": "#spi_preprocess_workflow.cwl/plot_size/outfile_name"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/select_size_max",
                            "id": "#spi_preprocess_workflow.cwl/plot_size/select_max"
                        },
                        {
                            "source": "#spi_preprocess_workflow.cwl/select_size_min",
                            "id": "#spi_preprocess_workflow.cwl/plot_size/select_min"
                        },
                        {
                            "valueFrom": "$(inputs.select_min + \":\" + inputs.select_max)",
                            "id": "#spi_preprocess_workflow.cwl/plot_size/selection"
                        }
                    ],
                    "out": [
                        "#spi_preprocess_workflow.cwl/plot_size/outfile"
                    ],
                    "id": "#spi_preprocess_workflow.cwl/plot_size"
                }
            ],
            "outputs": [
                {
                    "type": "File",
                    "outputSource": "#spi_preprocess_workflow.cwl/image_center/estimate_center_report",
                    "id": "#spi_preprocess_workflow.cwl/estimate_center_report"
                },
                {
                    "type": "File",
                    "outputSource": "#spi_preprocess_workflow.cwl/combine/outfile",
                    "id": "#spi_preprocess_workflow.cwl/outfile"
                },
                {
                    "type": "File",
                    "outputSource": "#spi_preprocess_workflow.cwl/image_center/refine_center_report",
                    "id": "#spi_preprocess_workflow.cwl/refine_center_report"
                },
                {
                    "type": "File",
                    "outputSource": "#spi_preprocess_workflow.cwl/image_center/rough_size_hist",
                    "id": "#spi_preprocess_workflow.cwl/rough_size_hist"
                },
                {
                    "type": "File",
                    "outputSource": "#spi_preprocess_workflow.cwl/plot_score/outfile",
                    "id": "#spi_preprocess_workflow.cwl/score_hist"
                },
                {
                    "type": "File",
                    "outputSource": "#spi_preprocess_workflow.cwl/plot_size/outfile",
                    "id": "#spi_preprocess_workflow.cwl/size_hist"
                }
            ],
            "id": "#spi_preprocess_workflow.cwl"
        }
    ],
    "cwlVersion": "v1.0"
}