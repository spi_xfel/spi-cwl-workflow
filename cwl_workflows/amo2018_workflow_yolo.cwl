#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  MultipleInputFeatureRequirement: {}

inputs: 
  input_files_1: File[]
  input_files_2: File[]
  estimate_size_r_min: int?
  estimate_size_r_max: int?
  estimate_size_min: int?
  estimate_size_max: int?
  estimate_size_ico: boolean
  refine_center_size_min: float
  refine_center_size_max: float
  select_size_min: float
  select_size_max: float
  select_score_min: float
  wavelength: float
  distance: float
  pixel_size: float
  class_dataset: string
  dragonfly_num_div: int?
  dragonfly_beta_start: float?
  dragonfly_beta_step: int?
  dragonfly_beta_mul: float?
  dragonfly_min_r: int?
  dragonfly_min_r_soft: int?
  dragonfly_max_r: int?
  dragonfly_binning: int?
  dragonfly_num_iter: int
  emc_correct_mode: string
  emc_correct_limit: int?
  pynx_num_run: int
  pynx_threshold: float?
  pynx_target_size: int
  pynx_algorithm: string?
  pynx_averaging_mode: string?

steps:
  preprocess_1:
    run: spi_preprocess_workflow.cwl
    in:
      input_files: input_files_1
      estimate_size_r_min: estimate_size_r_min
      estimate_size_r_max: estimate_size_r_max
      estimate_size_min: estimate_size_min
      estimate_size_max: estimate_size_max
      estimate_size_ico: estimate_size_ico
      refine_center_size_min: refine_center_size_min
      refine_center_size_max: refine_center_size_max
      select_size_min: select_size_min
      select_size_max: select_size_max
      select_score_min: select_score_min
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile, estimate_center_report, refine_center_report, rough_size_hist, size_hist, score_hist]
  preprocess_2:
    run: spi_preprocess_workflow.cwl
    in:
      input_files: input_files_2
      estimate_size_r_min: estimate_size_r_min
      estimate_size_r_max: estimate_size_r_max
      estimate_size_min: estimate_size_min
      estimate_size_max: estimate_size_max
      estimate_size_ico: estimate_size_ico
      refine_center_size_min: refine_center_size_min
      refine_center_size_max: refine_center_size_max
      select_size_min: select_size_min
      select_size_max: select_size_max
      select_score_min: select_score_min
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfile, estimate_center_report, refine_center_report, rough_size_hist, size_hist, score_hist]
  combine:
    run: ../cwl_steps/combine_data.cwl
    in:
      input_files:
        source: [ preprocess_1/outfile, preprocess_2/outfile ]
        linkMerge: merge_flattened
      outfile_name: 
        valueFrom: particle_size.cxi
    out: [outfile]
  yolo_class:
    run: ../cwl_steps/yolo_class.cwl
    in:
      input_file: combine/outfile
      dataset: class_dataset
    out: [outfile]
  class_filter:
    run: ../cwl_steps/filter_file.cwl
    in:
      input_file: yolo_class/outfile
      dataset: class_dataset
      min_value: { default: 1 }
    out: [outfile]
  emc_create:
    run: ../cwl_steps/dragonfly_create.cwl
    in:
      input_file: class_filter/outfile
      num_div: dragonfly_num_div
      beta_start: dragonfly_beta_start
      beta_step: dragonfly_beta_step
      beta_mul: dragonfly_beta_mul
      min_r: dragonfly_min_r
      min_r_soft: dragonfly_min_r_soft
      max_r: dragonfly_max_r
      binning: dragonfly_binning
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outdir]
  emc_run:
    run: ../cwl_steps/dragonfly_run.cwl
    in:
      recon_dir: emc_create/outdir
      num_iter: dragonfly_num_iter
    out: [outdir]
  emc_extract:
    run: ../cwl_steps/dragonfly_extract.cwl
    in:
      recon_dir: emc_run/outdir
    out: [outfile]
  emc_correct:
    run: ../cwl_steps/correct_emc.cwl
    in:
      input_file: emc_extract/outfile
      mode: emc_correct_mode
      limit: emc_correct_limit
    out: [outfile, report]
  pynx_run:
    run: ../cwl_steps/pynx_run.cwl
    in:
      input_file: emc_correct/outfile
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
      input_binning: dragonfly_binning
      num_run: pynx_num_run
      threshold: pynx_threshold
      target_size: pynx_target_size
      algorithm: pynx_algorithm
      averaging_mode: pynx_averaging_mode
    out: [outfile, report]
outputs:
  outfile:
    type: File
    outputSource: pynx_run/outfile
  estimate_center_report_1:
    type: File
    outputSource: preprocess_1/estimate_center_report
  refine_center_report_1:
    type: File
    outputSource: preprocess_1/refine_center_report
  rough_size_hist_1:
    type: File
    outputSource: preprocess_1/rough_size_hist
  size_hist_1:
    type: File
    outputSource: preprocess_1/size_hist
  score_hist_1:
    type: File
    outputSource: preprocess_1/score_hist
  estimate_center_report_2:
    type: File
    outputSource: preprocess_2/estimate_center_report
  refine_center_report_2:
    type: File
    outputSource: preprocess_2/refine_center_report
  rough_size_hist_2:
    type: File
    outputSource: preprocess_2/rough_size_hist
  size_hist_2:
    type: File
    outputSource: preprocess_2/size_hist
  score_hist_2:
    type: File
    outputSource: preprocess_2/score_hist
  emc_correct_report:
    type: File
    outputSource: emc_correct/report
  pynx_report:
    type: File
    outputSource: pynx_run/report
