#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}

inputs:
  input_files: File[]
  estimate_size_r_min: int?
  estimate_size_r_max: int?
  estimate_size_min: int?
  estimate_size_max: int?
  estimate_size_ico: boolean
  refine_center_size_min: float
  refine_center_size_max: float
  wavelength: float
  distance: float
  pixel_size: float

steps:
  estimate_center:
    run: ../cwl_steps/estimate_center.cwl
    in:
      input_files: input_files
    out: [outfiles, report]
  estimate_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file: estimate_center/outfiles
      min_psd_r: estimate_size_r_min
      max_psd_r: estimate_size_r_max
      min_size: estimate_size_min
      max_size: estimate_size_max
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
      icosahedron: estimate_size_ico
    out: [outfile]
  plot_histogram:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: particle_size_rough.pdf
      dataset:
        valueFrom: psd/size
      select_min: refine_center_size_min
      select_max: refine_center_size_max
      selection:
        valueFrom: $(inputs.select_min + ":" + inputs.select_max)
    out: [outfile]
  filter_size:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: estimate_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: refine_center_size_min
      max_value: refine_center_size_max
    out: [outfiles]
  refine_center:
    run: ../cwl_steps/refine_center.cwl
    in:
      input_files: filter_size/outfiles
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfiles, report]
  select_first_file:
    run:
      class: ExpressionTool
      inputs:
        files: File[]
      expression: '${return {"file": inputs.files[0]};}'
      outputs:
        file: File
    in:
      files: refine_center/outfiles
    out: [file]
  set_center:
    run: ../cwl_steps/set_center.cwl
    scatter: input_file
    in:
      input_file: input_files
      reference_file:
        select_first_file/file
    out: [outfile]
outputs:
  outfiles:
    type: File[]
    outputSource: set_center/outfile
  estimate_center_report:
    type: File
    outputSource: estimate_center/report
  refine_center_report:
    type: File
    outputSource: refine_center/report
  rough_size_hist:
    type: File
    outputSource: plot_histogram/outfile
