#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}

inputs: 
  input_files: File[]
  estimate_size_r_min: int?
  estimate_size_r_max: int?
  estimate_size_min: int?
  estimate_size_max: int?
  estimate_size_ico: boolean
  select_size_min: float
  select_size_max: float
  select_score_min: float
  wavelength: float
  distance: float
  pixel_size: float
  class_dataset: string
  em_class_num_iter: int
  em_class_qmin: int?
  em_class_qmax: int?
  em_class_num_rot: int?
  em_class_num_class: int?
  em_class_binning: int?
  em_class_symmetry_fringe: int?
  em_class_symmetry_order: int
  em_clash_thresh:
    type: float?
    default: 0.5
  em_clash_2_thresh:
    type: float?
    default: 0.75
  dragonfly_num_div: int?
  dragonfly_beta_start: float?
  dragonfly_beta_step: int?
  dragonfly_beta_mul: float?
  dragonfly_min_r: int?
  dragonfly_min_r_soft: int?
  dragonfly_max_r: int?
  dragonfly_binning: int?
  dragonfly_num_iter: int
  emc_correct_mode: string
  emc_correct_limit: int?
  pynx_num_run: int
  pynx_threshold: float?
  pynx_target_size: int
  pynx_algorithm: string?
  pynx_averaging_mode: string?

steps: 
  correct_bg:
    run: ../cwl_steps/correct_bg.cwl
    scatter: input_file
    in:
      input_file: input_files
    out: [outfile, report]
  estimate_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file: correct_bg/outfile
      min_psd_r: estimate_size_r_min
      max_psd_r: estimate_size_r_max
      min_size: estimate_size_min
      max_size: estimate_size_max
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
      icosahedron: estimate_size_ico
    out: [outfile]
  plot_size:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: particle_size.pdf
      dataset: 
        valueFrom: psd/size
      select_min: select_size_min
      select_max: select_size_max
      selection:
        valueFrom: $(inputs.select_min + ":" + inputs.select_max)
    out: [outfile]
  plot_score:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: particle_size_score.pdf
      dataset: 
        valueFrom: psd/size_score
    out: [outfile]
  filter_size:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: estimate_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: select_size_min
      max_value: select_size_max
    out: [outfiles]
  filter_score:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: filter_size/outfiles
      dataset:
        valueFrom: psd/size_score
      min_value: select_score_min
    out: [outfiles]
  combine:
    run: ../cwl_steps/combine_data.cwl
    in:
      input_files: filter_score/outfiles
      outfile_name: 
        valueFrom: particle_size.cxi
    out: [outfile]
  em_class:
    run: ../cwl_steps/em_class.cwl
    in:
      input_file: combine/outfile
      dataset: class_dataset
      num_iter: em_class_num_iter
      qmin: em_class_qmin
      qmax: em_class_qmax
      num_rot: em_class_num_rot
      num_class: em_class_num_class
      binning: em_class_binning
      symmetry_fringe: em_class_symmetry_fringe
      symmetry_order: em_class_symmetry_order
      threshold: em_clash_thresh
    out: [outfile, report]
  class_filter:
    run: ../cwl_steps/filter_file.cwl
    in:
      input_file: em_class/outfile
      dataset: class_dataset
      min_value: { default: 1 }
    out: [outfile]
  em_class_2:
    run: ../cwl_steps/em_class.cwl
    in:
      input_file: class_filter/outfile
      dataset: class_dataset
      report_name: { default: "em_class_report_2.pdf"}
      num_iter: em_class_num_iter
      qmin: em_class_qmin
      qmax: em_class_qmax
      num_rot: em_class_num_rot
      num_class: em_class_num_class
      binning: em_class_binning
      symmetry_fringe: em_class_symmetry_fringe
      symmetry_order: em_class_symmetry_order
      threshold: em_clash_2_thresh
    out: [outfile, report]
  class_filter_2:
    run: ../cwl_steps/filter_file.cwl
    in:
      input_file: em_class_2/outfile
      dataset: class_dataset
      min_value: { default: 1 }
    out: [outfile]
  emc_create:
    run: ../cwl_steps/dragonfly_create.cwl
    in:
      input_file: class_filter_2/outfile
      num_div: dragonfly_num_div
      beta_start: dragonfly_beta_start
      beta_step: dragonfly_beta_step
      beta_mul: dragonfly_beta_mul
      min_r: dragonfly_min_r
      min_r_soft: dragonfly_min_r_soft
      max_r: dragonfly_max_r
      binning: dragonfly_binning
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outdir]
  emc_run:
    run: ../cwl_steps/dragonfly_run.cwl
    in:
      recon_dir: emc_create/outdir
      num_iter: dragonfly_num_iter
    out: [outdir]
  emc_extract:
    run: ../cwl_steps/dragonfly_extract.cwl
    in:
      recon_dir: emc_run/outdir
    out: [outfile]
  emc_correct:
    run: ../cwl_steps/correct_emc.cwl
    in:
      input_file: emc_extract/outfile
      mode: emc_correct_mode
      limit: emc_correct_limit
    out: [outfile, report]
  pynx_run:
    run: ../cwl_steps/pynx_run.cwl
    in:
      input_file: emc_correct/outfile
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
      input_binning: dragonfly_binning
      num_run: pynx_num_run
      threshold: pynx_threshold
      target_size: pynx_target_size
      algorithm: pynx_algorithm
      averaging_mode: pynx_averaging_mode
    out: [outfile, report]
outputs:
  outfile:
    type: File
    outputSource: pynx_run/outfile
  size_hist:
    type: File
    outputSource: plot_size/outfile
  score_hist:
    type: File
    outputSource: plot_score/outfile
  em_class_report:
    type: File
    outputSource: em_class/report
  em_class_report_2:
    type: File
    outputSource: em_class_2/report
  emc_correct_report:
    type: File
    outputSource: emc_correct/report
  pynx_report:
    type: File
    outputSource: pynx_run/report
