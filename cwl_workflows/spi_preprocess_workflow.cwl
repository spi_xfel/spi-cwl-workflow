#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}
  ScatterFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  InlineJavascriptRequirement: {}

inputs: 
  input_files: File[]
  estimate_size_r_min: int?
  estimate_size_r_max: int?
  estimate_size_min: int?
  estimate_size_max: int?
  estimate_size_ico: boolean
  refine_center_size_min: float
  refine_center_size_max: float
  select_size_min: float
  select_size_max: float
  select_score_min: float
  wavelength: float
  distance: float
  pixel_size: float

steps:
  correct_bg:
    run: ../cwl_steps/correct_bg.cwl
    scatter: input_file
    in:
      input_file: input_files
    out: [outfile, report]
  image_center:
    run: image_center.cwl
    in:
      input_files: correct_bg/outfile
      estimate_size_r_min: estimate_size_r_min
      estimate_size_r_max: estimate_size_r_max
      estimate_size_min: estimate_size_min
      estimate_size_max: estimate_size_max
      estimate_size_ico: estimate_size_ico
      refine_center_size_min: refine_center_size_min
      refine_center_size_max: refine_center_size_max
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
    out: [outfiles, estimate_center_report, refine_center_report, rough_size_hist]
  estimate_size:
    run: ../cwl_steps/estimate_size.cwl
    scatter: input_file
    in:
      input_file: image_center/outfiles
      min_psd_r: estimate_size_r_min
      max_psd_r: estimate_size_r_max
      min_size: estimate_size_min
      max_size: estimate_size_max
      wavelength: wavelength
      distance: distance
      pixel_size: pixel_size
      icosahedron: estimate_size_ico
    out: [outfile]
  plot_size:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: particle_size.pdf
      dataset: 
        valueFrom: psd/size
      select_min: select_size_min
      select_max: select_size_max
      selection:
        valueFrom: $(inputs.select_min + ":" + inputs.select_max)
    out: [outfile]
  plot_score:
    run: ../cwl_steps/plot_histogram.cwl
    in:
      input_files: estimate_size/outfile
      outfile_name:
        valueFrom: particle_size_score.pdf
      dataset: 
        valueFrom: psd/size_score
    out: [outfile]
  filter_size:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: estimate_size/outfile
      dataset:
        valueFrom: psd/size
      min_value: select_size_min
      max_value: select_size_max
    out: [outfiles]
  filter_score:
    run: ../cwl_steps/filter_data.cwl
    in:
      input_files: filter_size/outfiles
      dataset:
        valueFrom: psd/size_score
      min_value: select_score_min
    out: [outfiles]
  combine:
    run: ../cwl_steps/combine_data.cwl
    in:
      input_files: filter_score/outfiles
      outfile_name: 
        valueFrom: particle_size.cxi
    out: [outfile]
outputs:
  outfile:
    type: File
    outputSource: combine/outfile
  estimate_center_report:
    type: File
    outputSource: image_center/estimate_center_report
  refine_center_report:
    type: File
    outputSource: image_center/refine_center_report
  rough_size_hist:
    type: File
    outputSource: image_center/rough_size_hist
  size_hist:
    type: File
    outputSource: plot_size/outfile
  score_hist:
    type: File
    outputSource: plot_score/outfile
