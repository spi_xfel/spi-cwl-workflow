#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Perform EM clustering on CXI data
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_em_class
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: |-
          spi_em_create.py $(inputs.input_file.path)
          spi_em_config.py --q_min $(inputs.qmin) --q_max $(inputs.qmax) --no-logscale --no-best --friedel --num_rot $(inputs.num_rot) --num_class $(inputs.num_class) --binning $(inputs.binning)
          spi_em_run.py $(inputs.num_iter)
          spi_em_report.py -o $(inputs.report_name)
          spi_em_save_by_symmetry.py $(inputs.symmetry_order) -f $(inputs.symmetry_fringe) -s $(inputs.dataset) -o $(inputs.outfile_name) -t $(inputs.threshold)
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_file:
    type: File
  report_name:
    type: string?
    default: em_class_report.pdf
  outfile_name:
    type: string?
    default: em_class.cxi
  dataset:
    type: string
    default: em_class/select
  symmetry_fringe:
    type: int?
    default: 0
  symmetry_order:
    type: int
  num_iter:
    type: int 
  qmin:
    type: int?
    default: 0
  qmax:
    type: int?
    default: 0
  num_rot:
    type: int?
    default: 100
  num_class:
    type: int?
    default: 20
  binning:
    type: int?
    default: 1
  threshold:
    type: float?
    default: 0.3
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outfile_name)
  report:
    type: File
    outputBinding:
      glob: $(inputs.report_name)
