#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Correct Dragonfly output in CXI file 
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_correct_amplitude
baseCommand: spi_correct.py
inputs:
  input_file:
    type: File
    inputBinding:
      position: 0
  outfile_name:
    type: string?
    default: emc_correct.cxi
    inputBinding:
      prefix: -o
  mode:
    type: string
    default: const
    inputBinding:
      prefix: -m
  limit:
    type: int?
    inputBinding:
      prefix: -l
  report_name:
    type: string?
    default: correct_emc_report.pdf
    inputBinding:
      prefix: -r
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outfile_name)
  report:
    type: File
    outputBinding:
      glob: $(inputs.report_name)
