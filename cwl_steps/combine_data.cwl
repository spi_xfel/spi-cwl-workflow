#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Combine several CXI files into one
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_combine.py
inputs:
  input_files:
    type: File[]
    inputBinding:
      position: 0
  outfile_name:
    type: string
    inputBinding:
      prefix: -o
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outfile_name)
