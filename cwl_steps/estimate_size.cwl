#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Estimate particle sizes for images in CXI file
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_estimate_size.py
inputs:
  input_file:
    type: File
    inputBinding:
      position: 0
  outdir_name:
    type: string?
    default: estimate_size_output
    inputBinding:
      prefix: -o
  min_psd_r:
    type: int?
    inputBinding:
      prefix: -r
  max_psd_r:
    type: int?
    inputBinding:
      prefix: -R
  min_size:
    type: int?
    inputBinding:
      prefix: -m
  max_size:
    type: int?
    inputBinding:
      prefix: -M
  wavelength:
    type: float
    inputBinding:
      prefix: --wavelength
  distance:
    type: float
    inputBinding:
      prefix: --distance
  pixel_size:
    type: float
    inputBinding:
      prefix: --pix
  icosahedron:
    type: boolean?
    default: false
    inputBinding:
      prefix: --icosahedron
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outdir_name)/$(inputs.input_file.basename)
