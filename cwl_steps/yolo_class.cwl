#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Perform YOLO based classification on CXI data
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_yolo_class
baseCommand: spi_yolo_class.py
inputs:
  input_file:
    type: File
    inputBinding:
      position: 0
  outfile_name:
    type: string?
    default: yolo_class.cxi
    inputBinding:
      prefix: -o
  area:
    type: string?
    default: 390,630,389,512
    inputBinding:
      prefix: --area
  dataset:
    type: string?
    default: yolo_class/select
    inputBinding:
      prefix: --dset
  yolo_size:
    type: int?
    default: 416
    inputBinding:
      prefix: --size
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outfile_name)
