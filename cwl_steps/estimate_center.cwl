#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Compute beam position in CXI file
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_estimate_center.py
inputs:
  input_files:
    type: File[]
    inputBinding:
      position: 0
  same_position:
    type: boolean?
    default: true
    inputBinding:
      prefix: -s
  outdir_name:
    type: string?
    default: estimate_center_output
    inputBinding:
      prefix: -o
  report_name:
    type: string?
    default: estimate_center_report.pdf
    inputBinding:
      prefix: -r
outputs:
  outfiles:
    type: File[]
    outputBinding:
      glob: $(inputs.outdir_name)/*
  report:
    type: File
    outputBinding:
      glob: $(inputs.report_name)
