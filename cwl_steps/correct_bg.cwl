#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Correct background in CXI file
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_correct_background.py
inputs:
  input_file:
    type: File
    inputBinding:
      position: 0
  outdir_name:
    type: string?
    default: correct_bg_output
    inputBinding:
      prefix: -o
  report_name:
    type: string?
    default: correct_background_report.pdf
    inputBinding:
      prefix: -r

outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outdir_name)/$(inputs.input_file.basename)
  report:
    type: File
    outputBinding:
      glob: $(inputs.report_name)

