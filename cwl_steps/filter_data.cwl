#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Filter set of CXI files by min_value <= dataset <= max_value rule.
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_filter.py
inputs:
  input_files:
    type: File[]
    inputBinding:
      position: 0
  outdir_name:
    type: string?
    default: filter_data_output
    inputBinding:
      prefix: -o
  dataset:
    type: string
    inputBinding:
      prefix: -d
  min_value:
    type: float?
    inputBinding:
      prefix: -m
  max_value:
    type: float?
    inputBinding:
      prefix: -M
outputs:
  outfiles:
    type: File[]
    outputBinding:
      glob: $(inputs.outdir_name)/*
