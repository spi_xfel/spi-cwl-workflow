#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Refine beam position in CXI file with sphere scattering pattern
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_refine_center.py
inputs:
  input_files:
    type: File[]
    inputBinding:
      position: 0
  outdir_name:
    type: string?
    default: refine_center_output
    inputBinding:
      prefix: -o
  same_position:
    type: boolean?
    default: true
    inputBinding:
      prefix: -s
  wavelength:
    type: float
    inputBinding:
      prefix: --wavelength
  distance:
    type: float
    inputBinding:
      prefix: --distance
  pixel_size:
    type: float
    inputBinding:
      prefix: --pix
  report_name:
    type: string?
    default: refine_center_report.pdf
    inputBinding:
      prefix: -r
outputs:
  outfiles:
    type: File[]
    outputBinding:
      glob: $(inputs.outdir_name)/*
  report:
    type: File
    outputBinding:
      glob: $(inputs.report_name)
