#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Plot histogram of values in dataset in CXI file
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_plot_histogram.py
inputs:
  input_files:
    type: File[]
    inputBinding:
      position: 0
  outfile_name:
    type: string?
    default: histogram.pdf
    inputBinding:
      prefix: -o
  dataset:
    type: string
    inputBinding:
      prefix: -d
  selection:
    type: string?
    inputBinding:
      prefix: -s
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outfile_name)

