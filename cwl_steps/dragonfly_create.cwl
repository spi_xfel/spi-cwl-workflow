#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Create Dragonfly working folder from CXI file
hints:
  DockerRequirement:
    dockerPull: spixfel/dragonfly_wrapper
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: >-
          dragonfly_create.py $(inputs.outdir_name) --num-div $(inputs.num_div) 
          --beta $(inputs.beta_start) $(inputs.beta_mul) $(inputs.beta_step) ;
          dragonfly_add.py $(inputs.input_file.path) $(inputs.outdir_name) 
          -r $(inputs.min_r) --rsoft $(inputs.min_r_soft) -R $(inputs.max_r) 
          -b $(inputs.binning) --wavelength $(inputs.wavelength)
          --distance $(inputs.distance) --pix $(inputs.pixel_size) --hdf ;
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_file:
    type: File
  outdir_name:
    type: string?
    default: data_emc
  num_div:
    type: int?
    default: 8
  beta_start:
    type: float?
    default: 0.01
  beta_step:
    type: int?
    default: 15
  beta_mul:
    type: float?
    default: 1.2
  min_r:
    type: int?
    default: 0
  min_r_soft:
    type: int?
    default: 0
  max_r:
    type: int?
    default: 10000
  binning:
    type: int?
    default: 1
  wavelength:
    type: float
  distance:
    type: float
  pixel_size:
    type: float
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
