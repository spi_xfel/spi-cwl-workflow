#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Phase retrieval for CXI data with PYNX lib
hints:
  DockerRequirement:
    dockerPull: spixfel/pynx-wrapper
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: >-
          pynx_create.py --data $(inputs.input_file.path) --nb_run $(inputs.num_run) \
          --detector_distance $(inputs.distance) --wavelength $(inputs.wavelength) \
          --pixel_size_detector $(inputs.pixel_size) --input_binning $(inputs.input_binning) \
          -o $(inputs.params_name) --support_threshold $(inputs.threshold) \
          --target_size $(inputs.target_size) --algorithm "$(inputs.algorithm)" && \
          pynx-id01cdi.py $(inputs.params_name) && \
          rm latest.cxi && \
          pynx_mean_result.py *.cxi -o $(inputs.outfile_name) --mode $(inputs.averaging_mode) -r $(inputs.report_name)
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  input_file:
    type: File
  outfile_name:
    type: string?
    default: result.cxi
  report_name:
    type: string?
    default: phase_retrieval.pdf
  wavelength:
    type: float
  distance:
    type: float
  pixel_size:
    type: float
  input_binning:
    type: int?
    default: 1
  num_run:
    type: int
  threshold:
    type: float?
    default: 0.25
  target_size:
    type: int
  algorithm:
    type: string?
    default: (Sup*ER**10) ** 20 * (Sup*HIO**90) * (Sup*ER**10) ** 20 * (Sup*HIO**90) * (Sup*ER**10) ** 20 * (Sup*HIO**90)
  params_name:
    type: string?
    default: pynx_params.txt
  averaging_mode:
    type: string?
    default: mean_real
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outfile_name)
  report:
    type: File
    outputBinding:
      glob: $(inputs.report_name)
