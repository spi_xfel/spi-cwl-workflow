#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Set beam position in CXI file
hints:
  DockerRequirement:
    dockerPull: spixfel/spi_processing
baseCommand: spi_set_center.py
inputs:
  input_file:
    type: File
    inputBinding:
      position: 0
  x:
    type: float?
    inputBinding:
      prefix: -x
  y:
    type: float?
    inputBinding:
      prefix: -y
  z:
    type: float?
    inputBinding:
      prefix: -z
  reference_file:
    type: File?
    inputBinding:
      prefix: -i
  outdir_name:
    type: string?
    default: set_center_output
    inputBinding:
      prefix: -o
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outdir_name)/$(inputs.input_file.basename)
