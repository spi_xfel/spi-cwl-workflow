#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Dragonfly computation on input data folder
hints:
  DockerRequirement:
    dockerPull: spixfel/dragonfly
requirements:
  InitialWorkDirRequirement:
    listing:
      - entryname: run.sh
        entry: >-
          cp $(inputs.recon_dir.path) $(inputs.outdir_name) -rf ;
          cd $(inputs.outdir_name) ;
          /src/emc -t `nproc` $(inputs.num_iter) ;
baseCommand: ["/bin/sh", "run.sh"]
inputs:
  recon_dir:
    type: Directory
  outdir_name:
    type: string
    default: data_emc
  num_iter:
    type: int
outputs:
  outdir:
    type: Directory
    outputBinding:
      glob: $(inputs.outdir_name)
