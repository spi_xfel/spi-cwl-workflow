#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Save Dragonfly result 3D amplitude into CXI file 
hints:
  DockerRequirement:
    dockerPull: spixfel/dragonfly_wrapper
baseCommand: dragonfly_extract.py
inputs:
  recon_dir:
    type: Directory
    inputBinding:
      position: 0
  outfile_name:
    type: string?
    default: dragonfly_result.cxi
    inputBinding:
      position: 1
  iter:
    type: int?
    inputBinding:
      prefix: -i
outputs:
  outfile:
    type: File
    outputBinding:
      glob: $(inputs.outfile_name)
